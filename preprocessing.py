#!/usr/bin/env python
#------------------------------------------------------------------------
#
#  preprocessing.py
#
#  Script to load the entire sample library, extract the input representations
#  and store them as objects in a pkl with all the associated metadata.
#
#  NOTES:
#     - representation is frame-wise log mag spectrogram, binned into 128 bark bands,
#       additional scaling for hearing reponse.
#     - only taking the first ~1.5s of each sound, or zero padding shorter
#       sounds to ~1.5s (128 frames * 512 hop)
#
#  Copyright (c) Adib Mehrabi, 2018
#  All rights reserved.
#
#------------------------------------------------------------------------

import numpy as np
import os
import pickle
import argparse
import soundfile as sf
import helper
import sample

def main():

    # Build the sample objects. 1 per audio file.
    # Each containing the input representation (barkgrams).
    # First create some vars that will be used by all sounds for the barkgrams.
    audio_dir = 'audio/'
    n_fft = 4096
    hop_size = 512
    n_bands = 128
    fs = 44100.0

    bark_basis = get_bark_basis(fs, n_fft, n_bands)
    bark_freqs = calc_bark_spaced_cent_freqs(n_bands)
    ear_basis = ear_model_basis(bark_freqs[1:-1])

    # Build the objects
    samples = []
    for dirpath, dirnames, filenames in os.walk(audio_dir):
        for filename in filenames:
            if filename[-3:] == 'wav':
                print(os.path.join(dirpath, filename))
                obj = sample.Sample(os.path.join(dirpath, filename), n_fft, hop_size, n_bands, bark_basis, bark_freqs, ear_basis)
                samples.append(obj)

    # Save them
    with open('preprocessed.pkl', 'wb') as output:
        pickler = pickle.Pickler(output, -1)
        pickler.dump(samples)


def get_bark_basis(fs, n_fft, n_bands):
    ''' Creates a Filterbank matrix to transform FFT bins into bark bins
    '''
    # Init the weight matrix
    weights = np.zeros((n_bands, int(1 + n_fft // 2)))

    # Calculate the bark frequencies
    bark_freqs = calc_bark_spaced_cent_freqs(n_bands)

    # Get the bin frequencies
    bin_freqs = bin_frequencies(fs, n_fft)

    # Calculate the bandwidths
    fdiff = np.diff(bark_freqs)

    # Calculate the lower and upper edges for all bins
    ramps = np.subtract.outer(bark_freqs, bin_freqs)

    # Calculate scaling to ensure bgram is approx constant energy per channel
    enorm = np.min(fdiff) / fdiff

    # Now calculate the weights
    for i in range(n_bands):
        lower = -ramps[i] / fdiff[i]
        upper = ramps[i + 2] / fdiff[i + 1]
        # and intersect them with each other and zero
        weights[i] = np.maximum(0, np.minimum(lower, upper)) * enorm[i]

    return weights


def calc_bark_spaced_cent_freqs(n_bands):
    ''' For a given number of bands, calculates the bark spaced frequencies from 0-24.9 barks
        (approx fs/2 at 44.1k).

        As we use triangular overlapping windows, each cutoff is a centre frequencie for the
        previous band, so we just take the cutoff frequencies to be centre frequencies.

        We calculate n_bands+2 centre frequencies as the first and last are only used as cutoffs
        in the basis matrix.

        Equations taken from: (Traunmüller 1990):
        https://ccrma.stanford.edu/courses/120-fall-2003/lecture-5.html

        NOTE: there is a discontinuity in the bandwidths when jumps at z >= 2 and z > 20.1.
        This does not matter on the low end, but does on the high end (there is a slight dip in bandwidth)
    '''

    # list of bark values
    barks = np.linspace(0,24.9,n_bands+2)
    cent_freqs = []

    # now set the rest of the cent freqs based on the calculation for bark cutoff frequencies
    for z in barks:
        if z < 2.0:
            z_prime = 2.0 + ((z-2.0) / 0.85)
        elif z > 20.1:
            z_prime = 20.1 + ((z-20.1) / 1.22)
        else:
            z_prime = z
        freq = (1960.0 * (z_prime + 0.53)) / (26.28 - z_prime)
        cent_freqs.append(freq)

    return np.array(cent_freqs)


def ear_model_basis(freqs):
    ''' Calculates the basis function with which to multiply the frequency band magnitudes,
        based on equation of Terhardt's ear model (http://web.media.mit.edu/~tristan/phd/dissertation/chapter3.html)
    '''
    db_diffs = []

    for i in range(len(freqs)):
        f = freqs[i] * 0.001
        db_diffs.append((-3.64 * pow(f, -0.8)) + (6.5 * np.exp(-0.6 * pow(f - 3.3, 2.))) - (pow(10., -3.) * pow(f, 4.)))

    return np.array(db_diffs)


def bin_frequencies(fs, n_fft):
    ''' Calculates the center frequencies of the fft bins
    '''
    return (float(fs) / n_fft) * np.arange(1 + n_fft / 2)

if __name__ == '__main__':
    main()
