#!/usr/bin/env python
#------------------------------------------------------------------------
#
#  inputs.py
#
#  Data generator class for training the CNN.
#
#  Serves up batches with fixed 50/50 split of library sounds and imitations
#
#  Copyright (c) Adib Mehrabi, 2018
#  All rights reserved.
#
#------------------------------------------------------------------------

import numpy as np

class DataGen():

    def __init__(self, batch_size, sounds):
        self.batch_size = batch_size
        self.lib_sounds = [s for s in sounds if s.type != '_LIBRARY_']
        self.imit_sounds = [s for s in sounds if s.type == '_IMITATIONS_']
        self.n_lib_sounds = int(self.batch_size*0.5)
        self.n_imit_sounds = int(self.batch_size*0.5)

    def __next__(self):
        lib_batch = np.random.choice(self.lib_sounds, self.n_lib_sounds)
        imit_batch = np.random.choice(self.imit_sounds, self.n_imit_sounds)
        mbatch = np.concatenate((imit_batch, lib_batch))
        return ((np.array([m.bgram for m in mbatch]), np.array([m.bgram for m in mbatch])))
