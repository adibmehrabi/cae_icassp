#!/usr/bin/env python
#------------------------------------------------------------------------
#
#  training.py
#
#  Code for training the cnn autoencoders.
#
#  Main parameters for tuning are specified as args.
#
#  Copyright (c) Adib Mehrabi, 2018
#  All rights reserved.
#
#------------------------------------------------------------------------

# Set seeds - OPTIONAL, comment once testing done
import numpy as np
import random as rn

import keras
from keras import backend as K
from keras.models import Model
from keras.layers import Activation, Input, Reshape, Flatten
from keras.layers.convolutional import Conv2D, UpSampling2D
from keras.layers.normalization import BatchNormalization
from keras.callbacks import ModelCheckpoint

import os
import pickle
import argparse
import inputs
import helper
import sample


def main():

    # Parse args
    parser = argparse.ArgumentParser(description="Script to train convolutional auto-encoder on barkgram data")
    parser.add_argument('-t', dest="train_data", type=str,
                        help="training dataset", default="preprocessed.pkl")
    parser.add_argument('-m', dest="model_dir", type=str,
                        help="where to store the model", default="models")
    parser.add_argument('-l', dest="l1_k_shape", type=tuple,
                        help="kernal shape of l1 and l8", default=(5,3))
    parser.add_argument('-k', dest="k_shape", type=tuple,
                        help="kernal shape of all other layers", default=(10,10))
    parser.add_argument('-s', dest="n_strides", type=list,
                        help="number of strides (per layer)", default=[(4,4), (4,4), (2,2), (2,2)])

    args = parser.parse_args()

    train_data = args.train_data
    model_dir = args.model_dir
    l1_k_shape = args.l1_k_shape
    k_shape = args.k_shape
    n_strides = args.n_strides

    # Load the preprocessed data
    print('Loading dataset...')
    with open(train_data, 'rb') as input:
        training_data = pickle.load(input)

    # Normalise and expand dims
    smax = np.max([d.bgram for d in training_data])
    smin = np.min([d.bgram for d in training_data])
    for i in range(len(training_data)):
        spec = (training_data[i].bgram - smin) / (smax - smin)
        training_data[i].bgram = np.expand_dims(spec, axis=2)

    # Get the train/valid splits. Note, we want a 70/30 split,
    # ensuring the same split of imitations and library sounds in each
    imits = [d for d in training_data if d.type == '_IMITATIONS_']
    libs = [d for d in training_data if d.type == '_LIBRARY_']
    rn.shuffle(imits)
    rn.shuffle(libs)
    train_sounds = libs[:int(len(libs)*0.7)] + imits[:int(len(imits)*0.7)]
    valid_sounds = libs[int(len(libs)*0.7):] + imits[int(len(imits)*0.7):]

    # Set batch size and steps per epoch
    batch_size = 128
    steps_per_epoch = int(len(train_sounds) / batch_size)
    validation_steps = int(len(valid_sounds) / batch_size)

    # Mini batch split 50/37.5/12/5 (libs/imits/stims)
    print('Creating generators...')
    train_gen = inputs.DataGen(batch_size, train_sounds)
    valid_gen = inputs.DataGen(batch_size, valid_sounds)

    print('Fitting model... ')
    sae_model = get_model(k_shape, l1_k_shape, n_strides)
    print(sae_model.summary())

    # Save model with init weights
    sae_model.save(model_dir + '/_init.h5')

    early_stopper = keras.callbacks.EarlyStopping(monitor='val_loss', patience=10, verbose=0, mode='auto')

    # For saving the model at each epoch improvement:
    filepath = model_dir + '/improvement_epoch_{epoch:02d}_{val_loss:.4f}' + '.h5'

    checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, mode='min', save_best_only=True)
    hist = sae_model.fit_generator(train_gen, steps_per_epoch, epochs=99,
                                   callbacks=[early_stopper, checkpoint],
                                   validation_data=valid_gen,
                                   validation_steps=validation_steps)

    print('Saving model and history...')
    sae_model.save(model_dir + '/final.h5')
    with open(model_dir + '/history', 'wb') as output:
        pickle.dump(hist.history, output)



def get_model(k_shape, l1_k_shape, n_strides):
    ''' 8 layer model with upsampling and conv with stride 1 on output layers.
    '''
    # Input
    inp = Input(shape=(128, 128, 1))  # height, width, channels (128 bark bins, zero padded/trunced to 128 frames)
    # Layer 1
    x = Conv2D(filters=8, kernel_size=l1_k_shape, strides=n_strides[0], padding='same')(inp)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    # Layer 2
    x = Conv2D(filters=16, kernel_size=k_shape, strides=n_strides[1], padding='same')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    # Layer 3
    x = Conv2D(filters=24, kernel_size=k_shape, strides=n_strides[2], padding='same')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    # Layer 4
    x = Conv2D(filters=32, kernel_size=k_shape, strides=n_strides[3], padding='same')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    # Encoded
    encoded_height = int(x.shape[1])
    encoded_width = int(x.shape[2])
    encoded_x = Flatten(name='encoded')(x)
    # Prepare for output layers
    y = Reshape(target_shape=(encoded_height, encoded_width, 32))(encoded_x)
    # Layer 5
    y = UpSampling2D(size=n_strides[3])(y)
    y = Conv2D(filters=32, kernel_size=k_shape, strides=(1, 1), padding='same')(y)
    y = BatchNormalization()(y)
    y = Activation('relu')(y)
    # Layer 6
    y = UpSampling2D(size=n_strides[2])(y)
    y = Conv2D(filters=24, kernel_size=k_shape, strides=(1, 1), padding='same')(y)
    y = BatchNormalization()(y)
    y = Activation('relu')(y)
    # Layer 7
    y = UpSampling2D(size=n_strides[1])(y)
    y = Conv2D(filters=16, kernel_size=l1_k_shape, strides=(1, 1), padding='same')(y)
    y = BatchNormalization()(y)
    y = Activation('relu')(y)
    # Layer 8
    y = UpSampling2D(size=n_strides[0])(y)
    y = Conv2D(filters=8, kernel_size=l1_k_shape, strides=(1, 1), padding='same')(y)
    y = BatchNormalization()(y)
    y = Activation('relu')(y)
    # Output
    y = Conv2D(filters=1, kernel_size=l1_k_shape, strides=(1, 1), padding='same')(y)
    y = BatchNormalization()(y)
    decoded = Activation('relu')(y)
    # Optimiser
    model = Model(inputs=inp, outputs=decoded)
    adam = keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    model.compile(optimizer=adam, loss='MSE')
    return model

if __name__ == '__main__':
    main()
