#!/usr/bin/env python
#------------------------------------------------------------------------
#
#  helper.py
#
#  Some functions for plotting stuff
#
#  Copyright (c) Adib Mehrabi, 2018
#  All rights reserved.
#
#------------------------------------------------------------------------

from matplotlib import pyplot as plt
import numpy as np
import random
import sample


#################### PLOTTING FUNCTIONS ####################

def plot_history(history, filepath = '~/Desktop/tmp', show=False):
    ''' Function to plit the history train/val loss for a single model.
        The filepath is where the plot will be saved.
    '''
    plt.plot(history['loss'])
    plt.plot(history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    if show == True:
        plt.show()
    else:
        plt.savefig(filepath +'.pdf')
    plt.clf()


def plot_bgram(bgram, filepath='~/Desktop/tmp', show=True):
    ''' Simply plots a 2d array of bgram (or spectrogram) frames
    '''
    plt.imshow(bgram, origin='lower', interpolation='nearest', aspect='auto')
    if show == True:
        plt.show()
    else:
        plt.savefig(filepath+'.pdf')
    plt.clf()


def plot_input_vs_output(inputs, model, filepath='~/Desktop/tmp', show=True):
    ''' Takes a model and an nparray of inputs (bgrams), with the same input shape used in training
        and plots the inputs with the predicted outputs
    '''
    outputs = model.predict(inputs)
    for i in range(inputs.shape[0]):
        fig = plt.figure()
        a=fig.add_subplot(1,2,1)
        v = outputs[i].reshape(128,128)
        plt.imshow(v, origin='lower', interpolation='nearest', aspect='auto')
        a=fig.add_subplot(1,2,2)
        s = inputs[i].reshape(128,128)
        plt.imshow(s, origin='lower', interpolation='nearest', aspect='auto')
        if show == True:
            plt.show()
        else:
            plt.savefig(filepath+'.pdf')
        plt.clf()
