#!/usr/bin/env python
#------------------------------------------------------------------------
#
#  sample.py
#
#  Class to store input representation for a given audio sample (file).
#  This generates and stores a barkgram (input features) with some associated metadata.
#
#  Copyright (c) Adib Mehrabi, 2018
#  All rights reserved.
#
#------------------------------------------------------------------------

import numpy as np
import soundfile as sf
import helper

class Sample(object):

    def __init__(self, filepath, n_fft, hop_size, n_bands, bark_basis, bark_freqs, ear_basis, plotting='off'):

        # Set sample metadata (from in dir structure)
        self.filepath = filepath
        split = self.filepath.split('/')
        self.type = split[1]
        self.filename = split[-1]

        # Load audio data
        data, self.fs = sf.read(filepath)
        if self.fs != 44100:
            print('FS ERROR !!!')

        # If it's stereo then sum and scale
        if len(data.shape) == 1:
            data = data
        elif len(data.shape) == 2:
            data = (data[:,0] + data[:,1]) * 0.5
        else:
            print('ERRROR IN WAV DATA')
            data = np.zeros(99)         # backup case - can filter these out later

        # We want a fixed size square input (CNN), so trim or pad the data to a fixed
        # length, (matching the y dim, i.e. n_bands)
        data = self.trim_and_pad(data, self.fs, n_fft, hop_size, n_bands)

        # Now build the barkgrams
        self.build_barkgram(data, self.fs, n_fft, hop_size, n_bands, bark_basis, bark_freqs, ear_basis, plotting)


    def trim_and_pad(self, data, fs, n_fft, hop_size, n_bands):

        # Force target length (n_bands frames after stft)
        target_length = (n_bands * hop_size) - (hop_size * 5)
        if len(data) > target_length:
            data = data[:target_length]

        # Apply tiny fade ins and outs if ness. (to prevent discontinuities)
        if data[0] != 0 or data[-1] != 0:
            fade_length = int(fs / 1000)
            fade_window = np.hanning(fade_length * 2)

            if data[0] != 0:
                fade = fade_window[:fade_length]
                data[:fade_length] *= fade

            if data[-1] != 0:
                fade = fade_window[fade_length:]
                data[-fade_length:] *= fade

        return data


    def build_barkgram(self, data, fs, n_fft, hop_size, n_bands, bark_basis, bark_freqs, ear_basis, plotting):

        # Get barkgram
        spec = self.stft(data, fs, n_fft, hop_size) # shape should be NxM, N=n_bins, M=n_frames
        bg = np.dot(bark_basis, spec)

        # Power to db - add eps to 0s to ensure no log(0) attempted
        smallest_val = np.nextafter(0, 1)
        bg[bg == 0.0] = smallest_val

        # Quick check that values are in range for logging
        if np.max(bg) > 1.0 or np.min(bg) <= 0.0:
            print('Range WARNING ', self.filepath, np.min(bg), np.max(bg))

        log_bg = 20 * np.log10(bg)

        # Apply ear scaling
        ear_bg = self.ear_model_scaling(log_bg, ear_basis)

        # Clip dynamic range
        clip_bg = np.clip(ear_bg, -144, 0)

        # Finally, 0 pad to 128 frames if needed
        if clip_bg.shape[1] < 128:
            padding = np.zeros((n_bands,n_bands-clip_bg.shape[1]))
            padding.fill(-144)
            clip_bg = np.concatenate((clip_bg, padding), axis=1)

        self.bgram = clip_bg

        # Shape check
        if clip_bg.shape != (128, 128):
            print('Shape WARNING ', self.filepath, self.bgram.shape)

        # Plot it (optional)
        if plotting != 'off':
            helper.plot_bgram(self.bgram)


    def stft(self, data, fs, n_fft, hop_size):
        ''' STFT function. Note: pads start and end to centre first window.
        '''
        data = np.insert(data,0,np.zeros(int(n_fft / 2)))                                    # start pad
        data = np.concatenate((data, np.zeros(hop_size-(len(data) % hop_size))))   # end pad (to make integer multiple of hop)
        data = np.concatenate((data, np.zeros(n_fft-hop_size)))    # end pad (to ensure all samples included in spec)
        total_frames = np.int32(len(data) / np.float32(hop_size))-np.int32(n_fft/hop_size)+1
        window = np.hanning(n_fft)                                                              # half cosine window
        spec = np.empty((total_frames, int(n_fft / 2) + 1), dtype=np.float32)                   # space to hold the spectrogram

        for i in range(total_frames):
            current_hop = hop_size * i
            frame = data[current_hop:current_hop+n_fft]         # get the current frame
            windowed = frame * window                           # apply window
            spectrum = np.fft.fft(windowed) / n_fft      # take the Fourier Transform and scale by n samples
            autopower = np.abs(spectrum * np.conj(spectrum))    # take the autopower spectrum
            spec[i, :] = autopower[:int(n_fft / 2) + 1]         # remove neg freqs
        return spec.T


    def ear_model_scaling(self, log_bg, ear_basis):
        ''' Applies basis function to input data (expects the input to be scaled in dB).
            Expects stft/bark/mel frames in NxM where N = freqs, and M = time frames.
        '''
        return (log_bg.T+ear_basis).T
