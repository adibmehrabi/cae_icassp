## Convolutional auto-encoder training code

This repo contains code for preprocessing audio files (into fixed size barkgram representations) and training a convolutional auto encoder. Training audio files should consist of both vocal imitations and real-world sounds (located in `audio/_IMITATIONS_` and `audio/_LIBRARY_` respectively).

It is a variant of the training code used in [1]:

[1]: Mehrabi, A., Choi, K., Dixon, S., & Sandler, M. (2018, April). Similarity measures for vocal-based drum sample retrieval using deep convolutional auto-encoders. In 2018 IEEE International Conference on Acoustics, Speech and Signal Processing (ICASSP) (pp. 356-360).


## Index

* **audio**: directory to include all audio files, split into vocal imitations `audio/_IMITATIONS_` and non-vocal imitations `audio/_LIBRARY_`. Expects audio in `.wav` format at 44.1kHz, stereo or mono. Note: all audio removed from this repository.
* **models**: directory to store the trained models, including initialised weights, states at each checkpoint and model history.


## Requirements

* Written using Python3. All requirements included in `requirements.txt`.


## Usage

To preprocess the audio files and create the training dataset (output in `.pkl` format):

```
    python3 preprocessing.py

```

To train the auto-encoder (optional args for tuning):

```
    python3 training.py

```
